@file:JsModule("pixi.js")
@file:JsNonModule

package PIXI

external object settings {
    var SCALE_MODE: Number
}