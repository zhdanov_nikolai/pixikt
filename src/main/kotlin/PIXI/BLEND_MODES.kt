@file:JsModule("pixi.js")
@file:JsNonModule

package PIXI

external object BLEND_MODES {
    val NORMAL: Number
    val ADD: Number
    val MULTIPLY: Number
    val SCREEN: Number
    val OVERLAY: Number
    val DARKEN: Number
    val LIGHTEN: Number
    val COLOR_DODGE: Number
    val COLOR_BURN: Number
    val HARD_LIGHT: Number
    val SOFT_LIGHT: Number
    val DIFFERENCE: Number
    val EXCLUSION: Number
    val HUE: Number
    val SATURATION: Number
    val COLOR: Number
    val LUMINOSITY: Number
    val NORMAL_NPM: Number
    val ADD_NPM: Number
    val SCREEN_NPM: Number
    val NONE: Number
    val SRC_IN: Number
    val SRC_OUT: Number
    val SRC_ATOP: Number
    val DST_OVER: Number
    val DST_IN: Number
    val DST_OUT: Number
    val DST_ATOP: Number
    val SUBTRACT: Number
    val SRC_OVER: Number
    val ERASE: Number
}