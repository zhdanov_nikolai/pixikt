@file:JsModule("pixi.js")
@file:JsNonModule

package PIXI

external open class BaseRenderTexture(
        width: Int = definedExternally,
        height: Int = definedExternally,
        scaleMode: Number = definedExternally,
        resolution: Int = definedExternally
)