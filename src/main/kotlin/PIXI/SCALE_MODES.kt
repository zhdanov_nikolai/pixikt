@file:JsModule("pixi.js")
@file:JsNonModule

package PIXI

external object SCALE_MODES {
    val LINEAR: Number
    val NEAREST: Number
}