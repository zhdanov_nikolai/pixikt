@file:JsModule("pixi.js")
@file:JsNonModule

package PIXI

external open class Sprite(texture: Texture) : DisplayObject, Mask {
    companion object {
        fun fromImage(imageUrl: String): Sprite
    }

    var texture: Texture
    var blendMode: Number
    var tint: Int
}