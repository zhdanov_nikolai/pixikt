@file:JsModule("pixi.js")
@file:JsNonModule

package PIXI

external open class Rectangle {
    var x: Number
    var y: Number
    var width: Number
    var height: Number
}