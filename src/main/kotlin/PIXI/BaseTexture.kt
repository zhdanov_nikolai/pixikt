@file:JsModule("pixi.js")
@file:JsNonModule

package PIXI

external open class BaseTexture {
    var scaleMode: Number
}